package com.company;
import java.util.Scanner;
import java.util.Arrays;
public class NameWeight {

    public static void main(String[] args) {
        Human[] humans = new Human[6];
        int[] weightarray = new int[6];
        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human();
            humans[i].setName();
            humans[i].setWeight();
            System.out.println(humans[i].getName());
            System.out.println(humans[i].getWeight());
            weightarray[i] = humans[i].getWeight();
        }
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 1; i < weightarray.length; i++) {
                if (weightarray[i] < weightarray[i - 1]) {
                    int temp = weightarray[i];
                    weightarray[i] = weightarray[i - 1];
                    weightarray[i - 1] = temp;
                    isSorted = false;
                }
            }
        }

        System.out.println(Arrays.toString(weightarray));

    }

}



